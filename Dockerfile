FROM php:7.4-apache-buster

ENV APACHE_DOCUMENT_ROOT /var/www/html/public/
ENV COMPOSER_ALLOW_SUPERUSER 1

WORKDIR /var/www/html/

RUN apt-get update -yqq && \
    apt-get dist-upgrade -yqq && \
    echo "Europe/Berlin" > /etc/timezone && \
    rm /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get install -yqq \
        libcurl4-gnutls-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libicu-dev \
        libpng-dev \
        libbz2-dev \
        gnupg2 \
        graphicsmagick \
        apt-transport-https \
        git \
        libxml2-dev \
        wget \
        unzip \
        libzip-dev \
        zlib1g-dev && \
    # Install PHP extensions
    docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install -j$(nproc) \
        intl \
        gd \
        zip \
        bz2 \
        opcache \
        mysqli && \
    echo "memory_limit=1024M\nalways_populate_raw_post_data = -1\nmax_execution_time = 240\nmax_input_vars = 1500\nupload_max_filesize = 32M\npost_max_size = 32M" > /usr/local/etc/php/conf.d/memory-limit.ini && \
    echo "date.timezone = Europe/Berlin" > /usr/local/etc/php/conf.d/timezone.ini && \
    pecl install \
        apcu && \
    docker-php-ext-enable \
        apcu && \
    docker-php-source delete && \
    # Node and yarn
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get update -yqq && \
    apt-get install -yqq \
        nodejs \
        yarn && \
    rm -rf /var/lib/apt/lists/* && \
    # Install and run Composer
    curl -sS https://getcomposer.org/installer | php